# WASM boilerplate

Skeleton setup for a WASM (C++) SPA.

## Development

Bunch of stuff is required to run this.
If you are on a Linux or MacOS, the chances are that you have `git`, `nodejs`, `default-jre`, `cmake`, ...
and just need the [emscripten](https://emscripten.org/docs/getting_started/downloads.html) toolchain:

```
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
git reset --hard master
./emsdk install 1.39.8
./emsdk activate 1.39.8
source ./emsdk_env.sh
```

The client WASM can be build by:
```
mkdir build
cd build
emcmake cmake ..
make clean
make
```

A simple server can be setup with:
```
cd public && python -m SimpleHTTPServer 3000
```

If you are not sure what is needed, look at the [Dockerfile](Dockerfile).

## Docker

The entire application is also available in a `Ubuntu 20.04` container:

```
docker build -t wasm_boilerplate:latest .
docker run -p 3000:3000 -t wasm_boilerplate:latest
```

... available on [http://localhost:3000/](http://localhost:3000/)


----

An more extensive example build the same way is done in [crest-calc](https://bitbucket.org/larsonvonh/crest-calc/src/master/).

