FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

# Update
RUN yes | apt-get update
RUN yes | apt-get upgrade

# Setup
RUN yes | apt-get install build-essential
RUN yes | apt-get install apt-utils
RUN yes | apt-get install libpthread-stubs0-dev
RUN yes | apt-get install curl
RUN yes | apt-get install unzip
RUN yes | apt-get install git
RUN yes | apt-get install cmake
RUN yes | apt-get install python

RUN yes | curl -sL https://deb.nodesource.com/setup_12.x | bash
RUN yes | apt-get install nodejs

RUN yes | apt-get install default-jre

# Start
WORKDIR /usr/src

# emscripten build
# ==========================================================================
RUN git clone https://github.com/emscripten-core/emsdk.git \
&& cd emsdk \
&& git reset --hard master
WORKDIR /usr/src/emsdk/
RUN ./emsdk install 1.39.8
RUN ./emsdk activate 1.39.8
# ==========================================================================

# Setup env and build code
# ==========================================================================
ARG APP_HOME=/var/www/wasm_boilerplate
WORKDIR /
RUN mkdir -p $APP_HOME/
ADD . $APP_HOME/
WORKDIR $APP_HOME/

RUN rm -rf build/
RUN rm -f public/index.js public/index.wasm public/asm-dom.js
RUN mkdir build/
RUN cd $APP_HOME/build/ \
&& PATH=$PATH:/usr/src/emsdk:/usr/src/emsdk/upstream/emscripten \
&& emcmake cmake -D CMAKE_BUILD_TYPE:STRING=production .. \
&& make

WORKDIR $APP_HOME/public/
CMD ["python", "-m", "SimpleHTTPServer", "3000"]
