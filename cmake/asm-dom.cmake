message(STATUS "[ASM-DOM] START")
include(FetchContent)
FetchContent_Declare(asm-dom
  GIT_REPOSITORY https://github.com/mbasso/asm-dom
  GIT_TAG 6c533f35dda7bd11db9d9b5167413a7f57a6c16c
)

FetchContent_GetProperties(asm-dom)
if(NOT asm-dom_POPULATED)
  message(STATUS "[ASM-DOM] setup")
  FetchContent_Populate(asm-dom)
  add_library(asm-dom
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom.cpp
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom-server.cpp
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom.hpp
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom-server.hpp
  )

  set_property(TARGET asm-dom PROPERTY CXX_STANDARD 14)
  target_include_directories(asm-dom PUBLIC ${asm-dom_SOURCE_DIR}/cpp/)
  configure_file(
    ${asm-dom_SOURCE_DIR}/dist/cpp/asm-dom.js
    ${PROJECT_SOURCE_DIR}/public/asm-dom.js
  )
endif()
message(STATUS "[ASM-DOM] DONE")
