find_program(gccx-found gccx)
if (NOT gccx-found)
  message("[GCCX] installing")
  execute_process(COMMAND npm install -g gccx)
else()
  message(STATUS "[GCCX] already installed")
endif()

function(parse_cpx source)
  get_filename_component(name ${CMAKE_CURRENT_SOURCE_DIR}/${source} NAME_WE)
  get_filename_component(dir  ${CMAKE_CURRENT_SOURCE_DIR}/${source} DIRECTORY)

  message(STATUS "[GCCX] file: ${CMAKE_CURRENT_SOURCE_DIR}/${source}")

  add_custom_command(
    OUTPUT
      ${dir}/${name}.cpp
    COMMAND
      gccx
    ARGS
      ${dir}/${name}.cpx
      -o ${dir}/${name}.cpp
    MAIN_DEPENDENCY
      ${dir}/${name}.cpx
  )
endfunction()