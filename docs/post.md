# Web clients in C++ 

## Web development

We are far from the days when the internet was modeled as a document exchange network.
We moved years back from the narrative of building web sites into the story of building web applications.

The underlying Javascript ecosystem, as the only natively supported programming language of the web,
is probably the most massive programming language in terms of its reach, and because of that,
its subjected to constant improvements, refinements, and finally, scrutiny
- not just as a way to make a programming language better but as a way to improve our web in general.

Web applications are usually a complicated and abstract set of libraries, interactions, code bits,
and various resources from all over the internet.
When doing actual work, one can be far removed from plain JS programming as developing can include a plethora of
libraries, frameworks, compilers, transcompilers, and building systems.
All of which solve some problems but are also guaranteed to introduce new ones.

Webassembly is a new addition to the mix.
This low-level language runs on the client and allows for almost native performance that is also
a portable compilation target for existing languages like C++, C, Rust, ...
Meaning that you can write code in C++, compile it to WASM and run it on the user's web client.

In terms of performance, regular, CSS-powered UI interfaces with typical user interactions do not need to be written in WASM.
However, if you are concerned with production performance or are doing non-trivial graphics, you can now do so on all major browsers and platforms.

In terms of our code practices on the web,
we have many new sources from languages that compile to WASM as a significant and production-tested set of libraries
are now also potentially accessible on the web.
Not everything can be easily compiled to WASM, and you can expect to have a lot of trouble while trying to compile complicated pieces of software.

## Overview

A small, proof-of-concept web client is written completely in C++ that shows that we can now write front-end code
completely in an language other than JavaScript.

For this, we use:

- [emscripten](https://emscripten.org/) is the toolchain for compiling C++ to WASM
- [asm-dom](https://github.com/mbasso/asm-dom) is a library for handling virtual DOM in C++
- [gccx](https://github.com/mbasso/gccx) (syntactic sugar around asm-dom) parses C++ files with JSX-like syntax to plain C++ language that regular compilers can handle
- [CMake](https://cmake.org/) a build system for C++ software
- [nodejs](https://nodejs.org/en/) is the server-side JS runtime
- [muparserx](https://github.com/beltoforion/muparserx) is a C++ math parser

The whole project is available in [the repository](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/), and its best to follow the code as one reads this.
The functionality of the client is simple:

![client_screen_1](client_screen_1.png)

- input element exists that, upon pressing enter, passes the entered string to a math expression parser
- the original expression and the result are added to the list
- the button exists that clears the list (but not the parser workspace)

One needs knowledge of programming in general and preferably C++ in particular (but not required as no fancy syntax was used) to follow.

## emscripten

Emscripten (`emcc`):

- compiles [LLVM](https://en.wikipedia.org/wiki/LLVM) bytecode from the `C`-language-family compiler [Clang](https://clang.llvm.org/)) into WASM
- generates JavaScript glue code

The `emcc` toolchain is easily installed directly from the source:
```
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
./emsdk install 1.39.8
./emsdk activate 1.39.8
source ./emsdk_env.sh
```

We use here a specific version, but one can, in general, use the latest:
```
./emsdk install latest
./emsdk activate latest
```

This will set up the required `PATH` and
```
emcc --version
```
... should output
```
emcc (Emscripten gcc/clang-like replacement) 1.39.8 (commit 14c0a9ec21193ffb9e2fce3fdabe1b598bfe4ef8)
...
```

## Build system

### asm-dom

ASM-DOM is a virtual DOM handler written for WASM/C++ and provides a way to manipulate DOM directly from your C++ code.
Here we install it in the [CMake build system](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/cmake/asm-dom.cmake):

```cmake
# asm-dom.cmake
message(STATUS "[ASM-DOM] START")
include(FetchContent)
FetchContent_Declare(asm-dom
  GIT_REPOSITORY https://github.com/mbasso/asm-dom
  GIT_TAG 6c533f35dda7bd11db9d9b5167413a7f57a6c16c
)

FetchContent_GetProperties(asm-dom)
if(NOT asm-dom_POPULATED)
  message(STATUS "[ASM-DOM] setup")
  FetchContent_Populate(asm-dom)
  add_library(asm-dom
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom.cpp
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom-server.cpp
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom.hpp
    ${asm-dom_SOURCE_DIR}/cpp/asm-dom-server.hpp
  )

  set_property(TARGET asm-dom PROPERTY CXX_STANDARD 14)
  target_include_directories(asm-dom PUBLIC ${asm-dom_SOURCE_DIR}/cpp/)
  configure_file(
    ${asm-dom_SOURCE_DIR}/dist/cpp/asm-dom.js
    ${PROJECT_SOURCE_DIR}/public/asm-dom.js
  )
endif()
message(STATUS "[ASM-DOM] DONE")
```

The source is downloaded via git and an `asm-dom` library created and set to compile to `asm-dom.js` in the public folder.
This `asm-dom.js` later needs to be loaded into the HTML page, so we compile it to the `public` folder.

### gccx

`gccx` is a parser that parses a JSX-like syntax in your C++ files into plain C++ that can get compiled by C compilers.
For example, we can write:

```
asmdom::VNode* image = <img src="hello.png" />;
```

... and pass it through `gccx` to get the `asm-dom` code (that might be too tedious to write by hand):

```c++
asmdom::VNode* image = asmdom::h(u8"img",
  asmdom::Data (
    asmdom::Attrs {
      {u8"src", u8"hello.png"}
    }
  )
);
```

This paradigm is much like we write DOM directly in JS/React.
We use a different file-ending `.cpx` for files that contain this syntax (like we use `.jsx` in JS/React).
GCCX is incorporated into the [CMake build system](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/cmake/gccx.cmake):

```cmake
# gccx.cmake
find_program(gccx-found gccx)
if (NOT gccx-found)
  message("[GCCX] installing")
  execute_process(COMMAND npm install -g gccx)
else()
  message(STATUS "[GCCX] already installed")
endif()

function(parse_cpx source)
  get_filename_component(name ${CMAKE_CURRENT_SOURCE_DIR}/${source} NAME_WE)
  get_filename_component(dir  ${CMAKE_CURRENT_SOURCE_DIR}/${source} DIRECTORY)

  message(STATUS "[GCCX] file: ${CMAKE_CURRENT_SOURCE_DIR}/${source}")

  add_custom_command(
    OUTPUT
      ${dir}/${name}.cpp
    COMMAND
      gccx
    ARGS
      ${dir}/${name}.cpx
      -o ${dir}/${name}.cpp
    MAIN_DEPENDENCY
      ${dir}/${name}.cpx
  )
endfunction()
```

We install `gccx` via npm and define a function `parsd_cpx` (in the context of CMake) that takes a `.cpx` source file and parses it to `.cpp` file in the same directory.

![gccx_compile_process](gccx_compile_process.png)

### CMake

To bring the [CMake build system](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/CMakeLists.txt) together:
```cmake
# CMakeLists.txt
cmake_minimum_required (VERSION 3.11)
project(wasm_boilerplate
  LANGUAGES CXX
  VERSION 0.1.0
)

# Include required CMake
include(cmake/release-mode.cmake)
include(cmake/gccx.cmake)
include(cmake/asm-dom.cmake)

# Parse the CPX files with gccx syntax into CPP
parse_cpx(src/components/command_input.cpx)
parse_cpx(src/components/results_list.cpx)
parse_cpx(src/components/clear_button.cpx)
parse_cpx(src/main.cpx)

# All the headers
file(GLOB TARGET_H
    src/*.h
    src/even_handlers/*.h
    src/state/*.h
    src/mup_parser/*.h
    src/components/*.h
)

# All the sources
file(GLOB TARGET_SRC
    src/*.cpp
    src/even_handlers/*.cpp
    src/state/*.cpp
    src/mup_parser/*.cpp
)

# Set the build to the public folder
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/public")
set(CMAKE_CXX_FLAGS "--bind -s LLD_REPORT_UNDEFINED -s WASM=1 -s EXPORTED_FUNCTIONS=\"['_main']\" -s EXTRA_EXPORTED_RUNTIME_METHODS=\"['ccall','cwrap','UTF8ToString']\"")

add_executable(index
    src/components/clear_button.cpp
    src/components/results_list.cpp
    src/components/command_input.cpp
    src/main.cpp
    ${TARGET_SRC} ${TARGET_H}
)

set_property(TARGET index PROPERTY CXX_STANDARD 14)
target_link_libraries(index PRIVATE asm-dom)
```

We parse all the `*.cpx` files (with `gccx` syntax) into `*.cpp` files with the function `gccx`.

The header and source GLOBs:
```cmake
# All the headers
file(GLOB TARGET_H
    ...
)

# All the sources
file(GLOB TARGET_SRC
    ...
)
```
... specify all the headers and source files via wildcard characters.
All the source code that we have is to be GLOB-ed here.

```cmake
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/public")
```
... sets the output to the `public` directory.
This compiler will output an `index.wasm` and the glue JS code `index.js`.
We only need to add the `index.js` in the page and it will take care of loading `index.wasm`.

```cmake
set(CMAKE_CXX_FLAGS "--bind -s LLD_REPORT_UNDEFINED -s WASM=1 -s EXPORTED_FUNCTIONS=\"['_main']\" -s EXTRA_EXPORTED_RUNTIME_METHODS=\"['ccall','cwrap','UTF8ToString']\"")
```
... sets the required `emcc` compiler options:

- `-s LLD_REPORT_UNDEFINED` enables detailed info about undefined symbols during compilation
- `-s WASM=1` enables WASM output
- `-s EXPORTED_FUNCTIONS="[...]"` defines the list of functions we want exported to the JS environment
- `-s EXTRA_EXPORTED_RUNTIME_METHODS="[...]"` defines the list of build-in functions we want to export to the JS environment

Finally, we create the `index` executable and link the build `asm-dom` library.

```cmake
add_executable(index
    src/components/clear_button.cpp
    src/components/results_list.cpp
    src/components/command_input.cpp
    src/main.cpp
    ${TARGET_SRC} ${TARGET_H}
)
```

We need to manually specify all the gccx `cpp` files since they do not exist until the `gccx` produces them.

[cmake/release-mode.cmake](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/cmake/release-mode.cmake) is an optional configuration to optimize the build in case of an `production` build:
```cmake
# cmake/release-mode.cmake
if (CMAKE_BUILD_TYPE STREQUAL "production")
  string(APPEND CMAKE_CXX_FLAGS " -Os")
  string(APPEND CMAKE_CXX_FLAGS " --js-opts 3")
  string(APPEND CMAKE_CXX_FLAGS " --llvm-lto 3")
  string(APPEND CMAKE_CXX_FLAGS " --llvm-opts 3")
  string(APPEND CMAKE_CXX_FLAGS " --closure 1")
endif()
```

To trigger this, we set the `-D CMAKE_BUILD_TYPE:STRING=production` ([in Docker](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/4e56b8aa764d5ffaef7ca5d6cd5065382f4a54e4/Dockerfile#lines-49)) while running CMake.

## Code

Once we have the build system, we can start looking at the code.

In the public folder, we have a simple [index.html](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/public/index.html):
```html
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>WASM boilerplate</title>
  <meta name="description" content="C++ (WASM) SPA boilerplate">

  <script src="asm-dom.js"></script>
  <script src="index.js"></script>
</head>

<body>
  <div id="root"></div>
</body>
</html>
```

- `asm-dom.js` is the compilation output of `asm-dom` library
- `index.js` is the emscripten-generated glue-code that binds the compiled `wasm`

### Math parser

The math parser's [source code](https://github.com/beltoforion/muparserx/tree/master/parser)
is directly copied into our source in [src/mup_parser](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/src/mup_parser/) and just referenced where needed.

### Main

Main virtual DOM Node named [view](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/4e56b8aa764d5ffaef7ca5d6cd5065382f4a54e4/src/main.cpx#lines-19) `asmdom::VNode` is created that represents our DOM:
```c++
asmdom::VNode *view;
```

We also init the [math parser instance](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/4e56b8aa764d5ffaef7ca5d6cd5065382f4a54e4/src/main.cpx#lines-16):
```c++
#include "./mup_parser/mpParser.h"

mup::ParserX math_parser(mup::pckALL_NON_COMPLEX);
```

As in all C++ programs, an `int main (int argc, char *argv[]) { ... }` function is the [entry point](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/4e56b8aa764d5ffaef7ca5d6cd5065382f4a54e4/src/main.cpx#lines-39).
```c++
int main(int argc, char *argv[]) {
  asmdom::Config config = asmdom::Config();
  asmdom::init(config);
  math_parser.EnableAutoCreateVar(true);

  emscripten::val root = document.call<emscripten::val>("getElementById", std::string("root"));
  view = <div/>;
  asmdom::patch(root, view);
  render();

  return 0;
}
```

There, we need to initialize and configure the `asm-dom`.
The `root` div is fetched and patched with our virtual Node.
Finally we call the render method.

### State

We declare the [state](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/src/state/) of our SPA:

```c++
#include <string>
#include <vector>

namespace STATE {

  extern std::string command_input_value;

  extern std::vector<std::vector<std::string>> list_of_command_entries;

}
```

### Components

We place each component in a separate `h/cpx` [file pair](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/src/components/).
For instance, the [command input](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/src/components/command_input.cpx):

```c++
namespace COMPONENTS {

  bool handle_command_input_key_up (emscripten::val event) {
    // Get the key code as integer
    // https://keycode.info/
    int key_code = event["keyCode"].as<int>();

    if (key_code == 13) {
      // Enter key
      std::string expr = event["target"]["value"].as<std::string>();
      math_parser.SetExpr(expr);
      mup::Value value = math_parser.Eval();
      STATE::list_of_command_entries.push_back({expr, value.ToString()});
      STATE::command_input_value = "";
    } else {
      STATE::command_input_value = event["target"]["value"].as<std::string>();
    }

    render();
    return false;
  }

  asmdom::VNode* get_command_input() {
    return (
      <div style="display:flex;flex-direction:column;">
        <label style="color:gray;">{"Enter a command and press Enter"}</label>
        <input
          id="command_input"
          style="padding:0 8px;height:32px;margin-bottom:12px;"
          type="text"
          value={STATE::command_input_value}
          (onkeyup)={handle_command_input_key_up} />
      </div>
    );
  };

}
```

The same happens for:

- [clear button](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/src/components/clear_button.cpx)

```c++
  namespace COMPONENTS {
  
    /**
     * Clear button `click` handler
     * @param event
     * @return
     */
    bool handle_clear_button_click (emscripten::val event);
  
    asmdom::VNode* get_clear_button ();
  
  }
```
  
- [result list](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/src/components/results_list.cpx)

```c++
  namespace COMPONENTS {
  
    /**
     * Get the DOM for the command input
     * @return
     */
    asmdom::VNode* get_result_list();
  
  }
```

### Render

Finally, the DOM is rendered in:

```c++
void render() {
  asmdom::VNode* new_node = (
    <div id="root" style="display:flex;flex-direction:column;width:50%;margin:0 auto;">
      { COMPONENTS::get_command_input() }
      { COMPONENTS::get_clear_button() }
      { COMPONENTS::get_result_list() }
    </div>
  );

  asmdom::patch(view, new_node);
  view = new_node;
}
```

We create a new virtual Node and patch it into our view.
Thus, each time render is called, this is evaluated and the DOM updated.
The update is smart and happens only for DOM elements that have changed.

![program cycle](program_cycle.png)

## Build and run

Finally, to run the code, we need to: 

- make the `build` directory `mkdir build && cd build`
- run emsdk cmake `emcmake cmake ..`
- run `make`

```
mkdir build
cd build
emcmake cmake ..
make
```

Make will produce `asm-dom.js`, `index.js` and `index.wasm` in the `public` folder.
One can now start a small server in the public folder to serve the content:

```
cd public
python -m SimpleHTTPServer 3000
```

... and finally visit [http://localhost:3000/](http://localhost:3000/)

The whole process is system agnostic in an accompanying [Docker container](https://bitbucket.org/9yards-agency/wasm_boilerplate/src/master/Dockerfile):

```
docker build -t wasm_boilerplate:latest .
docker run -p 3000:3000 -t wasm_boilerplate:latest
```

## Further reading

- [about emscripten](https://emscripten.org/docs/introducing_emscripten/about_emscripten.html)
- [awesome WASM](https://github.com/mbasso/awesome-wasm) is a curated list of WASM-related things
- [emscripten examples and demos](https://github.com/emscripten-core/emscripten/wiki/Porting-Examples-and-Demos)
