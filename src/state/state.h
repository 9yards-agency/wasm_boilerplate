#ifndef WASM_STATE_H
#define WASM_STATE_H

#include <string>
#include <vector>

namespace STATE {

  extern std::string command_input_value;

  extern std::vector<std::vector<std::string>> list_of_command_entries;

}


#endif //WASM_STATE_H
