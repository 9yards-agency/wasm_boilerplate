#ifndef WASM_BOILERPLATE_RESULTS_LIST_H
#define WASM_BOILERPLATE_RESULTS_LIST_H

#include <emscripten.h>
#include <emscripten/val.h>
#include "asm-dom.hpp"

#include "../state/state.h"

namespace COMPONENTS {

  /**
   * Get the DOM for the command input
   * @return
   */
  asmdom::VNode* get_result_list();

}

#endif //WASM_BOILERPLATE_RESULTS_LIST_H
