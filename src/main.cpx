#include <string>
#include <vector>
#include <iterator>
#include <emscripten.h>
#include <emscripten/val.h>
#include <iostream>
#include "asm-dom.hpp"

#include "./mup_parser/mpParser.h"
#include "./state/state.h"

#include "./components/command_input.h"
#include "./components/results_list.h"
#include "./components/clear_button.h"

mup::ParserX math_parser(mup::pckALL_NON_COMPLEX);

// Main view
asmdom::VNode *view;
emscripten::val document = emscripten::val::global("document");

/**
 * Render the DOM
 */
void render() {
  std::cout << "[WASM-BOILERPLATE] render view" << std::endl;
  asmdom::VNode* new_node = (
    <div id="root" style="display:flex;flex-direction:column;width:50%;margin:0 auto;">
      { COMPONENTS::get_command_input() }
      { COMPONENTS::get_clear_button() }
      { COMPONENTS::get_result_list() }
    </div>
  );

  asmdom::patch(view, new_node);
  view = new_node;
}

int main(int argc, char *argv[]) {
  std::cout << "[WASM-BOILERPLATE] loaded" << std::endl;
  asmdom::Config config = asmdom::Config();
  asmdom::init(config);
  std::cout << "[WASM-BOILERPLATE] ASM-DOM configured" << std::endl;
  math_parser.EnableAutoCreateVar(true);

  emscripten::val root = document.call<emscripten::val>("getElementById", std::string("root"));
  view = <div/>;
  asmdom::patch(root, view);
  render();

  return 0;
}
